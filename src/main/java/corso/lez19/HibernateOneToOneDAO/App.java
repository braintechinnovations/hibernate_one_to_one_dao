package corso.lez19.HibernateOneToOneDAO;

import corso.lez19.HibernateOneToOneDAO.models.Dipendente;
import corso.lez19.HibernateOneToOneDAO.models.DipendenteDAO;
import corso.lez19.HibernateOneToOneDAO.models.Indirizzo;
import corso.lez19.HibernateOneToOneDAO.models.IndirizzoDAO;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	DipendenteDAO dipDao = new DipendenteDAO();
    	IndirizzoDAO indDao = new IndirizzoDAO();
    	
    	Indirizzo ind = new Indirizzo("Piazza la bomba e scappa", "Rocca cannuccia", "AQ");
    	Dipendente dip = new Dipendente("DI7894", "Valeria Verdi");
    	dip.setIndirizzo(ind);
    	
    	indDao.insert(ind);
    	dipDao.insert(dip);
    	
    
    }
}
