package corso.lez19.HibernateOneToOneDAO.models;

import java.util.List;

import org.hibernate.Session;

import corso.lez19.HibernateOneToOneDAO.models.db.GestoreSessioni;

public class DipendenteDAO implements Dao<Dipendente> {

	@Override
	public void insert(Dipendente t) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Dipendente findById(int id) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			Dipendente temp = sessione.get(Dipendente.class, id);
			
			sessione.getTransaction().commit(); 

			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
		return null;
		
	}


	@Override
	public List<Dipendente> findAll() {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			List<Dipendente> elenco = sessione.createQuery("FROM Dipendente").list();
			
			sessione.getTransaction().commit(); 
			
			return elenco;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return null;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(int id) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();

			Dipendente temp = sessione.load(Dipendente.class, id);
			sessione.delete(temp);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(Dipendente t) {
		
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.delete(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean update(Dipendente t) {
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.update(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}
	
}
