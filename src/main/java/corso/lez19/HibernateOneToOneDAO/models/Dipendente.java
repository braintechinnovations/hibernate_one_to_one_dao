package corso.lez19.HibernateOneToOneDAO.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="dipendente")
public class Dipendente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dipendenteID")
	private int id;
	
	@Column
	private String codice_dip;
	@Column
	private String nominativo;
	
	@OneToOne
	@JoinColumn(name="indirizzo_rif", referencedColumnName = "indirizzoID")
	private Indirizzo indirizzo;

	public Dipendente() {
		
	}
	
	public Dipendente(String codice_dip, String nominativo) {
		super();
		this.codice_dip = codice_dip;
		this.nominativo = nominativo;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodice_dip() {
		return codice_dip;
	}

	public void setCodice_dip(String codice_dip) {
		this.codice_dip = codice_dip;
	}

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public Indirizzo getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}

	@Override
	public String toString() {
		return "Dipendente [id=" + id + ", codice_dip=" + codice_dip + ", nominativo=" + nominativo + ", indirizzo="
				+ indirizzo + "]";
	}
	
}
