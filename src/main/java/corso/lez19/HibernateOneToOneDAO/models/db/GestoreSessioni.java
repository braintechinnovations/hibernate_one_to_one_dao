package corso.lez19.HibernateOneToOneDAO.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.lez19.HibernateOneToOneDAO.models.Dipendente;
import corso.lez19.HibernateOneToOneDAO.models.Indirizzo;

public class GestoreSessioni {
	private static GestoreSessioni ogg_gestore;
	private SessionFactory factory;

	public static GestoreSessioni getIstanza() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}
	
	public SessionFactory getFactory() {
		if(factory == null) {
			factory = new Configuration()
					.configure("/resources/hibernate_dipendenti.cfg.xml")
					.addAnnotatedClass(Dipendente.class)
					.addAnnotatedClass(Indirizzo.class)
					.buildSessionFactory();
		}
		
		return factory;
	}
}
